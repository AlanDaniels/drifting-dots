
// TODO: NEXT, optimize this a bit. Don't draw the same lines twice.
// TODO: Order the lines by dot ID, and eliminate duplicates. Lines will need comparison support.

package com.alandaniels.driftingdots;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.Vector;
import java.util.Random;
import java.util.TreeSet;


class Dot
{
    private static final int BORDER = 100;

    private static final int WIDTH = Gdx.graphics.getWidth();

    private static final int HEIGHT = Gdx.graphics.getHeight();

    private static final float BASE_SPEED = 200.0f;

    private int _ID;
    private Vector2 _pos;
    private Vector2 _dir;

    public Dot(int ID, Random rand) {
        _ID = ID;
        float x = rand.nextFloat() * WIDTH;
        float y = rand.nextFloat() * HEIGHT;
        _pos = new Vector2(x, y);

        float speed = BASE_SPEED + (rand.nextFloat() * BASE_SPEED);
        _dir = new Vector2(speed, 0.0f);
        _dir.rotate(rand.nextFloat() * 360.0f);
    }

    // Update the position, and clamp it to the screen.
    public void onTick(float delta) {
        _pos.x += _dir.x * delta;
        _pos.y += _dir.y * delta;

        while (_pos.x < -BORDER) {
            _pos.x += WIDTH;
        }
        while (_pos.x > (WIDTH + BORDER)) {
            _pos.x -= WIDTH;
        }

        while (_pos.y < -BORDER) {
            _pos.y += HEIGHT;
        }
        while (_pos.y > (HEIGHT + BORDER)) {
            _pos.y -= HEIGHT;
        }
    }

    @Override
    public int hashCode() {
        return _ID;
    }

    int getID() { return _ID; }
    public Vector2 getPos() { return _pos; }
}


// Two dots, to and from.
class DotPair implements Comparable<DotPair>
{
    private Dot _first;
    private Dot _second;

    // When we assign first and second, go by ID order, for sorting later.
    public DotPair(Dot dotOne, Dot dotTwo) {
        if (dotOne.getID() < dotTwo.getID()) {
            _first  = dotOne;
            _second = dotTwo;
        } else {
            _first  = dotTwo;
            _second = dotOne;
        }
    }

    @Override
    public int compareTo(DotPair that) {
        int this1 = _first.getID();
        int this2 = _second.getID();

        int that1 = that._first.getID();
        int that2 = that._second.getID();

        if (this1 < that1) {
            return -1;
        } else if (this1 > that1) {
            return 1;
        } else if (this2 < that2) {
            return -1;
        } else if (this2 > that2) {
            return 1;
        } else {
            return 0;
        }
    }

    public Dot getFirst()  { return _first; }
    public Dot getSecond() { return _second; }
}



public class MyGdxGame extends ApplicationAdapter
{
    private static final int DOT_COUNT = 200;

    private static final float RADIUS = 5.0f;

	private Dot _dots[];

	private TreeSet<DotPair> _pairs;

	private OrthographicCamera _camera;

	private SpriteBatch _batch;

	private ShapeRenderer _shaper;

	@Override
	public void create() {
	    Random rand = new Random();

	    _pairs = new TreeSet<DotPair>();

		_dots = new Dot[DOT_COUNT];
		for (int i = 0; i < DOT_COUNT; i++) {
		    _dots[i] = new Dot(i, rand);
        }

		int width  = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		_camera = new OrthographicCamera();
		_camera.setToOrtho(false, width, height);

		_shaper = new ShapeRenderer();
        _batch = new SpriteBatch();
	}

	@Override
    public void render() {
        float delta = Gdx.graphics.getDeltaTime();
        onTick(delta);
        onRender();
    }

    public void onTick(float delta) {
	    // Move the dots.
        for (int i = 0; i < DOT_COUNT; i++) {
            _dots[i].onTick(delta);
        }

        // Recalc the closest pairs.
        _pairs.clear();

        for (int i = 0; i < DOT_COUNT; i++) {
            Dot first = _dots[i];

            // Walk the rest to find the closest dot.
            double closestDist = Double.MAX_VALUE;
            Dot closestDot = null;
            for (int j = 0; j < DOT_COUNT; j++) {
                if (j != i) {
                    Dot other = _dots[j];
                    double xdist = first.getPos().x - other.getPos().x;
                    double ydist = first.getPos().y - other.getPos().y;
                    double dist  = (xdist * xdist) + (ydist * ydist);
                    if (dist < closestDist) {
                        closestDist = dist;
                        closestDot  = other;
                    }
                }
            }

            if (closestDot != null) {
                _pairs.add(new DotPair(first, closestDot));
            }
        }
    }

	public void onRender() {
		Gdx.gl.glClearColor(0.0f, 0.1f, 0.3f, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        _batch.setProjectionMatrix(_camera.combined);
		_batch.begin();
		_shaper.begin(ShapeRenderer.ShapeType.Filled);
        _shaper.setColor(1.0f, 1.0f, 1.0f, 1.0f);

        for (DotPair pair : _pairs) {
            Dot first = pair.getFirst();
            Dot second = pair.getSecond();

            _shaper.line (first.getPos(), second.getPos());
        }

        for (int i = 0; i < DOT_COUNT; i++) {
            Vector2 pos = _dots[i].getPos();
            _shaper.circle(pos.x, pos.y, RADIUS);
        }

		_shaper.end();
		_batch.end();
	}
	
	@Override
	public void dispose() {
		_batch.dispose();
	}
}
