package com.alandaniels.driftingdots.desktop;

import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.alandaniels.driftingdots.MyGdxGame;

public class DesktopLauncher
{
	// Just default to the primary monitor's default resolution.
	public static void main (String[] arg) {
		Graphics.DisplayMode desktopMode = LwjglApplicationConfiguration.getDesktopDisplayMode();

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width  = desktopMode.width;
		config.height = desktopMode.height;
		config.fullscreen = true;
		new LwjglApplication(new MyGdxGame(), config);
	}
}
